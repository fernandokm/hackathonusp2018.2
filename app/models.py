from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=100)
    image = models.CharField(max_length=500)


class User(models.Model):
    name = models.CharField(max_length=100)
    image = models.CharField(max_length=500)


class Project(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=400)
    area = models.CharField(max_length=100)
    tema = models.CharField(max_length=100)
    subtema = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.CharField(max_length=500)

    def currencies(self):
        return Currency.objects.filter(project_id=self.pk)

    def equipments(self):
        return Equipment.objects.filter(project_id=self.pk)


class Resource(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Currency(Resource):
    amount = models.DecimalField(max_digits=20, decimal_places=2)


class Equipment(Resource):
    name = models.CharField(max_length=100)


class Transaction(models.Model):
    cost = models.DecimalField(max_digits=20, decimal_places=2)
    seller = models.CharField(max_length=100)


class Product(models.Model):
    name = models.CharField(max_length=100)
    cost = models.DecimalField(max_digits=20, decimal_places=2)
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
