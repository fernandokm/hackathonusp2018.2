from django.views import generic
from django.shortcuts import render, redirect, reverse, get_object_or_404
from .models import User, Project, Currency, Equipment, Company
from .forms import ProjectForm, ProjectSearchForm
from urllib import parse


class IndexView(generic.base.TemplateView):
    template_name = 'app/index.html'


def user(request, user_id):
    u = get_object_or_404(User, pk=user_id)

    if request.method == 'POST':
        print(request.POST)
        form = ProjectForm(request.POST)
        success = form.is_valid()

        if success:
            proj = Project.objects.create(title=form.cleaned_data['title'], description=form.cleaned_data['description'],
                                          area=form.cleaned_data['area'], tema=form.cleaned_data['tema'],
                                          subtema=form.cleaned_data['subtema'], image=form.cleaned_data['image'],
                                          user_id=user_id)
            if form.cleaned_data['currency'] != 0:
                Currency.objects.create(
                    amount=form.cleaned_data['currency'], project_id=proj.pk)
            for equip in form.cleaned_data['equipment'].split(','):
                equip = equip.strip()
                if equip != '':
                    Equipment.objects.create(name=equip, project_id=proj.pk)

        return redirect(reverse(f'app:user', args=[user_id]) + '?success=' + str(success).lower())

    success = request.GET.get('success', None)

    context = {
        'user': u,
        'projects': Project.objects.filter(user_id=user_id),
        'form': ProjectForm(),
        'submitted': success is not None,
        'success': success and (success.lower() == 'true'),
        'logged_in': True,
    }
    return render(request, 'app/user.html', context)


def projects(request):
    context = {
        'projects': Project.objects.all(),
    }
    return render(request, 'app/projects.html', context)


def company(request, company_id):
    c = get_object_or_404(Company, pk=company_id)
    projs = []

    if request.method == 'POST':
        print(request.POST)
        form = ProjectSearchForm(request.POST)
        if not form.is_valid():
            print(form.errors)
        return redirect(reverse(f'app:company', args=[company_id]) + '?' + parse.urlencode({
            'area': form.cleaned_data['area'],
            'tema': form.cleaned_data['tema'],
            'subtema': form.cleaned_data['subtema']}))

    area = request.GET.get('area', None)
    tema = request.GET.get('tema', None)
    subtema = request.GET.get('subtema', None)

    if {area, tema, subtema} != {None}:
        projs = Project.objects.filter(
            area__contains=area or '', tema__contains=tema or '', subtema__contains=subtema or '')
        no_matches = len(projs) == 0
    else:
        projs = []
        no_matches = False

    context = {
        'company': c,
        'projects': projs,
        'form': ProjectSearchForm(),
        'no_matches': no_matches,
        'logged_in': True,
        'show_projects': len(projs) > 0,
    }
    return render(request, 'app/company.html', context)
