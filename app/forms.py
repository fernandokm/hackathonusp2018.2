from django import forms


class ProjectForm(forms.Form):
    title = forms.CharField(label='Nome do projeto', max_length=100)
    description = forms.CharField(label='Descrição', max_length=400)
    area = forms.CharField(label='Área', max_length=100)
    tema = forms.CharField(label='Tema', max_length=100)
    subtema = forms.CharField(label='Subtema', max_length=100)
    currency = forms.DecimalField(label='Precisa de (R$)', decimal_places=2, max_digits=20, min_value=0)
    equipment = forms.CharField(label='Equipamentos')
    image = forms.CharField(label='Imagem', max_length=500)


class ProjectSearchForm(forms.Form):
    area = forms.CharField(label='Área', required=False)
    tema = forms.CharField(label='Tema', max_length=100, required=False)
    subtema = forms.CharField(label='Subtema', max_length=100, required=False)
