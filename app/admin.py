from django.contrib import admin
from . import models


class CurrencyInline(admin.TabularInline):
    model = models.Currency
    extra = 1


class EquipmentInline(admin.TabularInline):
    model = models.Equipment
    extra = 1


class ProjectAdmin(admin.ModelAdmin):
    fields = ['title', 'description', 'area', 'tema', 'subtema', 'image', 'user']
    inlines = [CurrencyInline, EquipmentInline]


admin.site.register(models.Project, ProjectAdmin)
admin.site.register(models.Transaction)
admin.site.register(models.User)
admin.site.register(models.Company)