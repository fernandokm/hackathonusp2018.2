from django.urls import path

from . import views

app_name = 'app'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('user/<int:user_id>?success=<success>', views.user, name='user'),
    path('user/<int:user_id>', views.user, name='user'),
    path('company/<int:company_id>', views.company, name='company'),
    path('projects', views.projects, name='projects'),
    # path(r'<int:pk>/', views.DetailView.as_view(), name='detail'),
    # path(r'<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # path(r'<int:question_id>/vote/', views.vote, name='vote'),
]
